import React from 'react';
import Card from "./Cards/Card";
import './App.css';

const SUITS = {
    DIAMONDS: 'D',
    HEARTS: 'H',
    CLUBS: 'C',
    SPADES: 'S'
};

const RANKS = {
    TWO: '2',
    THREE: '3',
    FOUR: '4',
    FIVE: '5',
    SIX: '6',
    SEVEN: '7',
    EIGHT: '8',
    NINE: '9',
    TEN: '10',
    JACK: 'J',
    QUEEN: 'Q',
    KING: 'K',
    ACE: 'A'
}

class App extends React.Component {
    state = {
        cards: [],
        hand: []
    }

    componentDidMount() {
        this.initGame();
    };

    initGame = () => {
        const cards = [];

        for (let suit in SUITS) {
            for (let rank in RANKS) {
                cards.push({
                    suit: SUITS[suit],
                    rank: RANKS[rank]
                });
            }
        }

        console.log(cards);

        this.setState(state => {
            return {...state, cards}
        });
    };

    getCard = () => {
        const cards = this.state.cards;  // [Card, Card , Card]
        const copyCards = [...cards];

        const randomIndex = Math.floor(Math.random() * copyCards.length)
        const removed = copyCards.splice(randomIndex, 1); // [el]

        const copyHand = [...this.state.hand];
        copyHand.push(removed[0]);

        this.setState({
            cards: copyCards,
            hand: copyHand
        });
    }

    getCards = (howMany) => {
        for (let i = 0; i < howMany; i++) {
            this.getCard();
        }
    }

    getFiveCards = () => {
        this.getCards(5);
    }

    render () {
        const deck = this.state.hand.map((card) => {
            return <Card suit={card.suit} rank={card.rank}/>
        });

        return (
            <div className="deck">
                <div className="card-deck">
                    <div>
                        {deck}
                    </div>
                </div>

                <div>
                    <button onClick={this.getFiveCards}>Get Five Cards</button>
                </div>
            </div>
        );
    }
}

export default App;
